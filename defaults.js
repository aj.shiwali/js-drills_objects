function defaults(obj, defaultProps) {
  for (var key in defaultProps) {
    if (obj[key]) {
      return obj;
    } else {
      obj[key] = defaultProps[key];
    }
  }
  return obj;
}
module.exports = defaults;
