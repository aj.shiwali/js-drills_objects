function invert(obj) {
  let cpyobj = {};
  for (var key in obj) {
    cpyobj[obj[key]] = key;
  }
  return cpyobj;
}
module.exports = invert;
